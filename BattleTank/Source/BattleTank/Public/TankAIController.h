// Copyright EmbraceIT Ltd.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankAIController : public AAIController
{
	GENERATED_BODY()

protected:
	// How close can the AI tank get
	UPROPERTY(EditDefaultsOnly, Category="Setup")
	float AcceptanceRadius = 8000.0f;
	
private:
	UFUNCTION()
	void OnPossessedTankDeath();

protected:
	ATankAIController();

	virtual void SetPawn(APawn* InPawn) override;

	virtual void Tick(float DeltaTime) override;
};
